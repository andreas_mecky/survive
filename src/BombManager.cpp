#include "BombManager.h"
#include "Constants.h"

// -------------------------------------------------------
// Init
// -------------------------------------------------------
void BombManager::init() {

	m_GameSettings = static_cast<GameSettings*>(m_World->getSettings("game"));
	assert(m_GameSettings != 0);
	float scale = m_GameSettings->bombRadius / 50.0f;
	for ( int x = 0; x < MAX_X_CELLS ; ++x ) {
		for ( int y = 0; y < MAX_Y_CELLS; ++y ) {
			m_Cells[x][y] = 0;
		}
	}

	for ( int i = 0; i < MAX_BOMBS; ++i ) {
		Gate* gate = &m_Gates[i];
		gate->active = true;
		gate->state = GS_ACTIVE;
		gate->timer = 0.0f;
		ds::Vec2 sp = getStartPosition(gate);
		if ( i < 3 ) {
			gate->orientation = 0;
			m_World->addSpriteEntity(2,0,&gate->firstCorner,sp.x,sp.y+59.0f,ds::Rect(120,32,32,32));
			m_World->addSpriteEntity(2,0,&gate->secondCorner,sp.x,sp.y-60.0f,ds::Rect(120,0,32,32));			
			for ( int j = 0; j < 9; ++j ) {
				Item* item = &gate->items[j];
				item->dir = 0;
				m_World->addSpriteEntity(2,0,&item->entity,sp.x,sp.y+10.0*j,ds::Rect(110,170,37,10));
			}
			setHorizontalGate(gate,sp);
		}
		else {
			gate->orientation = 1;
			m_World->addSpriteEntity(2,0,&gate->firstCorner,sp.x-59.0f,sp.y,ds::Rect(120,64,32,32));
			m_World->addSpriteEntity(2,0,&gate->secondCorner,sp.x+60.0f,sp.y,ds::Rect(120,96,32,32));
			float angle = 0.0f;
			for ( int j = 0; j < 9; ++j ) {
				Item* item = &gate->items[j];		
				item->dir = 1;
				m_World->addSpriteEntity(2,0,&item->entity,sp.x + 10.0f*j,sp.y,ds::Rect(12,160,10,37));		
			}
			setVerticalGate(gate,sp);
		}
		
		m_World->addSpriteEntity(2,0,&gate->ringEntity,sp.x,sp.y,ds::Rect(170,0,100,100),0.0f,scale,scale,ds::Color(1.0f,1.0f,1.0f,0.75f));
		gate->ringEntity.setActive(false);
		m_World->addCollisionEntity(2,&gate->boxCollisionEntity);
		gate->boxCollisionEntity.setPosition(sp);
		m_World->addCollisionEntity(2,&gate->expCollisionEntity);
		gate->expCollisionEntity.setActive(false);
		gate->expCollisionEntity.setPosition(sp);
		if ( i < 3 ) {
			m_World->setBoxShape(&gate->boxCollisionEntity,35.0f,140.0f,GATE_TYPE);
		}
		else {
			m_World->setBoxShape(&gate->boxCollisionEntity,140.0f,35.0f,GATE_TYPE);
		}
		m_World->setCircleShape(&gate->expCollisionEntity,m_GameSettings->bombRadius,BOMB_TYPE);
	}


}

// -------------------------------------------------------
// Set vertical gate
// -------------------------------------------------------
void BombManager::setVerticalGate(Gate* gate,const ds::Vec2& pos) {
	ds::Vec2 sp = pos;
	gate->firstCorner.setPosition(ds::Vec2(sp.x - 55.0f,sp.y));
	gate->secondCorner.setPosition(ds::Vec2(sp.x + 55.0f,sp.y));	
	float angle = 0.0f;
	for ( int j = 0; j < 9; ++j ) {
		Item* item = &gate->items[j];
		item->timer = 0.0f;
		float sx = 0.7f + sin(DEGTORAD(angle)) * 0.15f;
		item->entity.setPosition(ds::Vec2(sp.x - 40.0f + j * 10.0f,sp.y));
		item->angle = angle;
		angle += 40.0f;
	}
	gate->boxCollisionEntity.setPosition(sp);
	gate->expCollisionEntity.setPosition(sp);
	gate->ringEntity.setPosition(sp);
	gate->boxCollisionEntity.setActive(true);
}

// -------------------------------------------------------
// Set horizontal gate
// -------------------------------------------------------
void BombManager::setHorizontalGate(Gate* gate,const ds::Vec2& pos) {
	ds::Vec2 sp = pos;
	gate->firstCorner.setPosition(ds::Vec2(sp.x,sp.y + 55.0f));
	gate->secondCorner.setPosition(ds::Vec2(sp.x,sp.y - 55.0f));	
	float angle = 0.0f;
	for ( int j = 0; j < 9; ++j ) {
		Item* item = &gate->items[j];
		item->timer = 0.0f;
		float sx = 0.7f + sin(DEGTORAD(angle)) * 0.15f;
		item->entity.setPosition(ds::Vec2(sp.x,sp.y - 40.0f + j * 10.0f));
		item->angle = angle;
		angle += 40.0f;
	}
	gate->boxCollisionEntity.setPosition(sp);
	gate->expCollisionEntity.setPosition(sp);
	gate->ringEntity.setPosition(sp);
	gate->boxCollisionEntity.setActive(true);
}

// -------------------------------------------------------
// Activate gate
// -------------------------------------------------------
void BombManager::activateGate(int entityID) {
	for ( int i =0; i < MAX_BOMBS; ++i ) {
		Gate* gate = &m_Gates[i];
		if ( gate->state == GS_ACTIVE && gate->boxCollisionEntity.getID() == entityID ) {
			gate->state= GS_TICKING;
			gate->timer = 0.0f;
			gate->boxCollisionEntity.setActive(false);
			gate->ringEntity.setActive(true);
		}
	}
}

// -------------------------------------------------------
// Reset gate
// -------------------------------------------------------
void BombManager::resetGate(int entityID) {
	for ( int i =0; i < MAX_BOMBS; ++i ) {
		Gate* gate = &m_Gates[i];
		if ( gate->expCollisionEntity.getID() == entityID ) {
			gate->state = GS_ACTIVE;
			gate->timer = 0.0f;
			m_Cells[gate->gx][gate->gy] = 0;
			gate->boxCollisionEntity.setActive(true);
			gate->ringEntity.setActive(false);
			gate->expCollisionEntity.setActive(false);
			ds::Vec2 sp = getStartPosition(gate);
			if ( gate->orientation == 0 ) {
				setHorizontalGate(gate,sp);
			}
			else {
				setVerticalGate(gate,sp);
			}
		}
	}
}

// -------------------------------------------------------
// Reactivate all gates
// -------------------------------------------------------
void BombManager::reactivateAll() {
	for ( int i =0; i < MAX_BOMBS; ++i ) {
		Gate* gate = &m_Gates[i];
		if ( !gate->active ) {
			gate->state = GS_ACTIVE;
			gate->timer = 0.0f;
			m_Cells[gate->gx][gate->gy] = 0;
			gate->boxCollisionEntity.setActive(true);
			gate->ringEntity.setActive(false);
			gate->expCollisionEntity.setActive(false);
			ds::Vec2 sp = getStartPosition(gate);
			if ( gate->orientation == 0 ) {
				setHorizontalGate(gate,sp);
			}
			else {
				setVerticalGate(gate,sp);
			}			
			gate->firstCorner.setActive(true);
			gate->secondCorner.setActive(true);
			for ( int j = 0; j < 9; ++j ) {
				Item* item = &gate->items[j];		
				item->entity.setActive(true);
			}
		}
	}
}

// -------------------------------------------------------
// Get random start position
// -------------------------------------------------------
ds::Vec2 BombManager::getStartPosition(Gate* gate) {
	for ( int i = 0; i < 10; ++i ) {
		int gx = ds::math::random(0,5);
		int gy = ds::math::random(0,4);
		if ( m_Cells[gx][gy] == 0 ) {
			m_Cells[gx][gy] = 1;
			gate->gx = gx;
			gate->gy = gy;
			int wpx = 325 + gx * 150;
			int wpy = 250 + gy * 150;
			return ds::Vec2(wpx,wpy);
		}
	}
	// FIXME: we should not get here
	return ds::Vec2(800,600);
}

// -------------------------------------------------------
// Clear
// -------------------------------------------------------
void BombManager::clear() {
	for ( int i = 0; i < MAX_BOMBS; ++i ) {
		Gate* gate = &m_Gates[i];
		gate->active = false;
		gate->boxCollisionEntity.setActive(true);
		gate->ringEntity.setActive(false);
		gate->expCollisionEntity.setActive(false);
		gate->firstCorner.setActive(false);
		gate->secondCorner.setActive(false);
		for ( int j = 0; j < 9; ++j ) {
			Item* item = &gate->items[j];		
			item->entity.setActive(false);
		}
	}
}

// -------------------------------------------------------
// Get active positions
// -------------------------------------------------------
void BombManager::getActivePositions(std::vector<ds::Vec2>& positions) {
	for ( int i = 0; i < MAX_BOMBS; ++i ) {
		Gate* gate = &m_Gates[i];
		positions.push_back(gate->ringEntity.getPosition());
	}
}
// -------------------------------------------------------
// Update
// -------------------------------------------------------
void BombManager::update(float elapsed) {
	for ( int i = 0; i < MAX_BOMBS; ++i ) {
		Gate* gate = &m_Gates[i];
		if ( gate->state == GS_EXPLODING ) {
			gate->state = GS_ACTIVE;
			gate->timer = 0.0f;
			resetGate(gate->expCollisionEntity.getID());
		}	
		else if ( gate->state == GS_ACTIVE ) {
			gate->timer += elapsed;
			for ( int j = 0; j < 9; ++j ) {
				Item* item = &gate->items[j];
				float sx = m_GameSettings->gateSize + sin(DEGTORAD(item->angle)) * m_GameSettings->gateVariance;
				item->angle += 360.0f * elapsed * m_GameSettings->gateAmplitude;			
				if ( item->angle > 360.0f ) {
					item->angle -= 360.0f;
				}
				if ( item->dir == 0 ) {
					item->entity.setScale(sx,1.0f);
				}
				else {
					item->entity.setScale(1.0f,sx);
				}
			}
		}
		else if ( gate->state == GS_TICKING ) {			
			gate->timer += elapsed;
			float normTime = gate->timer / m_GameSettings->bombTTL;
			float baseScale = m_GameSettings->bombRadius / 50.0f;
			float add = baseScale / 10.0f;
			float scale = baseScale + sin(normTime * 4.0f) * add;
			gate->ringEntity.setScale(scale,scale);
			if ( gate->timer > m_GameSettings->bombTTL ) {
				gate->expCollisionEntity.setActive(true);	
				BombExplosion exp;
				exp.position = gate->ringEntity.getPosition();
				exp.radius = m_GameSettings->bombRadius;
				exp.id = gate->expCollisionEntity.getID();
				exp.dir = gate->orientation;
				m_Events.add(100,&exp,sizeof(BombExplosion));
				gate->state = GS_EXPLODING;
				gate->timer = 0.0f;
			}			
		}
	}
}
