#pragma once
#include <math\math_types.h>
#include <utils\Color.h>
#include <settings\DynamicSettings.h>

const int MAX_WORMS = 32;
const int MAX_STARS = 10;

const ds::Rect BLOB_RECT = ds::Rect(120,120,32,32);

const float WORM_TIMER = 10.0f;

const ds::Vec2 START_POS[8] = {ds::Vec2(180,920),ds::Vec2(600,920),ds::Vec2(1000,920),ds::Vec2(1000,600),ds::Vec2(1000,370),ds::Vec2(600,370),ds::Vec2(180,370),ds::Vec2(180,600)};

const int FILL_ARRAY[8][25] = {
	{ // top left corner
		1,1,1,0,0,
		1,1,0,0,0,
		1,1,0,0,0,
		1,0,0,0,0,
		0,0,0,0,0
	},
	{ // top center
		1,1,1,1,1,
		0,1,1,1,0,
		0,0,0,0,0,
		0,0,0,0,0,
		0,0,0,0,0
	},
	{ // top right corner
		0,0,1,1,1,
		0,0,0,1,1,
		0,0,0,1,1,
		0,0,0,0,1,
		0,0,0,0,0
	},
	{ // right
		0,0,0,0,1,
		0,0,0,1,1,
		0,0,0,1,1,
		0,0,0,1,1,
		0,0,0,0,1
	},
	{ // right bottom corner
		0,0,0,0,0,
		0,0,0,0,1,
		0,0,0,1,1,
		0,0,0,1,1,
		0,0,1,1,1
	},
	{ // bottom
		0,0,0,0,0,
		0,0,0,0,0,
		0,0,0,0,0,
		0,1,1,1,0,
		1,1,1,1,1
	},
	{ // bottom left corner
		0,0,0,0,0,
		1,0,0,0,0,
		1,1,0,0,0,
		1,1,0,0,0,
		1,1,1,0,0
	},
	{ // left
		1,0,0,0,0,
		1,1,0,0,0,
		1,1,0,0,0,
		1,1,0,0,0,
		1,0,0,0,0
	}
};


struct GameSettings : public ds::DynamicSettings {

	float bombRadius;
	float bombVelocity;
	float bombTTL;
	float sparkleVelocity;
	float sparklePickupRadius;
	float sparkleTargetVelocity;
	float cubeGrowTime;
	float gateSize;
	float gateAmplitude;
	float gateVariance;

	void load(NewSettingsReader& reader) {
		reader.get<float>("bomb_radius",&bombRadius);
		reader.get<float>("bomb_velocity",&bombVelocity);
		reader.get<float>("bomb_ttl",&bombTTL);
		reader.get<float>("sparkle_velocity",&sparkleVelocity);
		reader.get<float>("sparkle_pickup_radius",&sparklePickupRadius);
		reader.get<float>("sparkle_target_velocity",&sparkleTargetVelocity);
		reader.get<float>("cube_grow_time",&cubeGrowTime);
		reader.get<float>("gate_size",&gateSize);
		reader.get<float>("gate_amplitude",&gateAmplitude);
		reader.get<float>("gate_variance",&gateVariance);
	}
};

struct HUDData {

	int scoreIdx;
	int score;

};


struct BombExplosion {

	ds::Vec2 position;
	float radius;
	int id;
	int dir;
};

const int PLAYER_TYPE = 1;
const int CUBE_TYPE   = 2;
const int BOMB_TYPE   = 3;
const int RING_TYPE   = 4;
const int EXP_TYPE    = 5;
const int GATE_TYPE   = 6;