#pragma once
#include <world\World.h>
#include <renderer\render_types.h>
#include <world\SpriteEntity.h>
#include "Constants.h"
// -------------------------------------------------------
// Alien
// -------------------------------------------------------
enum AlienState {
	AS_GROWING,
	AS_MOVING
};

struct Alien {
	int id;
	ds::SpriteEntity entity;
	bool active;
	ds::Vec2 velocity;
	ds::Vec2 position;
	int type;
	AlienState state;
	float timer;
};

// -------------------------------------------------------
// Behaviour
// -------------------------------------------------------
class Behaviour {

public:
	Behaviour() {}
	virtual ~Behaviour() {}

	virtual bool update(Alien* currentAlien,Alien* otherAlien) = 0;

	const bool hasReacted() const {
		return m_Reacted;
	}
	const ds::Vec2& getReaction() const {
		return m_Reaction;
	}
	void reset() {
		m_Reacted = false;
		m_Reaction.x = 0.0f;
		m_Reaction.y = 0.0f;
	}	
protected:
	bool m_Reacted;
	ds::Vec2 m_Reaction;
};

// -------------------------------------------------------
// TargetBehaviour
// -------------------------------------------------------
class TargetBehaviour : public Behaviour {

public:
	TargetBehaviour(float targetSpeed) : Behaviour() , m_TargetSpeed(targetSpeed) {}
	virtual ~TargetBehaviour() {}
	bool update(Alien* currentAlien,Alien* otherAlien) {
		reset();
		ds::Vec2 diff = otherAlien->position - currentAlien->position;
		m_Reaction = ds::vector::normalize(diff);
		m_Reaction *= m_TargetSpeed;
		m_Reacted = true;
		return m_Reacted;
	}
private:
	float m_TargetSpeed;
};

// -------------------------------------------------------
// FleeBehaviour
// -------------------------------------------------------
class FleeBehaviour : public Behaviour {

public:
	FleeBehaviour() : Behaviour() {}
	virtual ~FleeBehaviour() {}
	bool update(Alien* currentAlien,Alien* otherAlien) {
		reset();
		ds::Vec2 diff = currentAlien->position - otherAlien->position;
		m_Reaction = ds::vector::normalize(diff);
		m_Reaction *= 50.0f;
		m_Reacted = true;
		return m_Reacted;
	}
};

// -------------------------------------------------------
// SeparationBehaviour
// -------------------------------------------------------
class SeparationBehaviour : public Behaviour {

public:
	SeparationBehaviour(float separationDistance) : Behaviour() , m_SeparationDistance(separationDistance) {}
	virtual ~SeparationBehaviour() {}
	bool update(Alien* currentAlien,Alien* otherAlien) {
		reset();
		if ( otherAlien != 0 ) {
			ds::Vec2 pushDirection = currentAlien->position - otherAlien->position;
			//if ( ds::vector::length(pushDirection) > 5.0f ) {
				m_Reaction = ds::vector::normalize(pushDirection);
				m_Reaction *= m_SeparationDistance;
				m_Reacted = true;
			//}
		}
		return m_Reacted;
	}
private:
	float m_SeparationDistance;
};

const int MAX_SWARM = 256;
// -------------------------------------------------------
// Swarm
// -------------------------------------------------------
class Swarm : public ds::GameObject {

public:
	Swarm();
	virtual ~Swarm(void);
	void setTargetPos(const ds::Vec2& targetPos) {
		m_TargetPos = targetPos;
	}
	void update(float elapsed);
	void init();
	int numAlive();
	void start(int type);
	void getActivePosition(std::vector<ds::Vec2>& positions);
	void clear();
	void removeByID(int entityID);
	void setStartCount(int startCount) {
		m_StartCount = startCount;
	}
	const int getStarted() const {
		return m_Started;
	}
private:
	void startGroups(int swarms);
	Alien* findClosest(const Alien& currentAlien);
	int findFreeAlien();
	ds::Sprite* m_Sprites[4];
	Alien m_Aliens[MAX_SWARM];
	Alien* m_Target;
	TargetBehaviour m_TargetBehaviour;
	FleeBehaviour m_FleeBehaviour;
	SeparationBehaviour m_SeparationBehaviour;
	ds::Vec2 m_TargetPos;
	GameSettings* m_GameSettings;
	int m_Waves;
	int m_StartCount;
	int m_Started;
	int m_Level;
};

