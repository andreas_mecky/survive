#pragma once
#include <world\World.h>
#include "Constants.h"
#include <world\SpriteEntity.h>

const int MAX_BOMBS = 6;
const int MAX_X_CELLS = 5;
const int MAX_Y_CELLS = 4;

class BombManager : public ds::GameObject {

enum GateState {
	GS_ACTIVE,
	GS_TICKING,
	GS_EXPLODING
};

struct Item {
	ds::SpriteEntity entity;
	float timer;
	float angle;
	int dir;
};

struct Gate {

	bool active;
	ds::Vec2 position;
	GateState state;
	ds::SpriteEntity firstCorner;
	ds::SpriteEntity secondCorner;
	Item items[9];
	ds::SpriteEntity ringEntity;
	float timer;
	int orientation;
	ds::CollisionEntity boxCollisionEntity;
	ds::CollisionEntity expCollisionEntity;
	int gx;
	int gy;
};

public:
	BombManager() : ds::GameObject() {}
	virtual ~BombManager() {}
	void update(float elapsed);
	void init();
	void updateHead(const ds::Vec2& headPos) {
		m_HeadPos = headPos;
	}
	void getActivePositions(std::vector<ds::Vec2>& positions);
	void clear();
	void activateGate(int entityID);
	void resetGate(int entityID);
	void reactivateAll();
private:		
	void setVerticalGate(Gate* gate,const ds::Vec2& pos);
	void setHorizontalGate(Gate* gate,const ds::Vec2& pos);
	ds::Vec2 getStartPosition(Gate* gate);
	ds::Vec2 getVelocity(const ds::Vec2& startPos);
	GameSettings* m_GameSettings;
	Gate m_Gates[MAX_BOMBS];
	int m_Cells[MAX_X_CELLS][MAX_Y_CELLS];
	ds::Vec2 m_HeadPos;
};

