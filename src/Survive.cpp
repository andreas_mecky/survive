#include "Survive.h"
#include "utils\Log.h"
#include <math\math_types.h>
#include "Constants.h"
#include <renderer\shader.h>
#include <utils\font.h>
#include <math\GameMath.h>
#include <utils\Profiler.h>


ds::BaseApp *app = new Survive(); 



Survive::Survive() : ds::BaseApp() {
	//_CrtSetBreakAlloc(1478);
	m_Width = 1024;
	m_Height = 768;
	m_ClearColor = ds::Color(0.0f,0.0f,0.0f,1.0f);		
	m_HeadPos = ds::Vec2(512,384);
}

Survive::~Survive() {
	delete m_DialogBatch;
}

// -------------------------------------------------------
// Load content and prepare game
// -------------------------------------------------------
bool Survive::loadContent() {	

	audio->loadSound("BombExplosion");
	audio->loadSound("Pickup");
	audio->loadSound("Explosion");

	renderer->createVertexBuffer(ds::PT_TRI,ds::VD_PTC,65536,true);
	renderer->createIndexBuffer(98304,true);

	m_AddBS = renderer->createBlendState(ds::BL_ONE,ds::BL_ONE,true);	

	m_World.setCamera(0,&m_Camera);
	m_World.setCamera(1,&m_Camera);
	m_World.setCamera(2,&m_Camera);
	int id = m_World.createSpriteBatch("TextureArray");	

	

	m_World.loadSettings("game",&m_GameSettings);

	m_World.addSpriteEntity(0,0,&m_Background,800,600,ds::Rect(0,512,512,384),0.0f,3.125f,3.125f);

	m_World.addSpriteEntity(3,0,&m_HeadRingEntity,800,600,ds::Rect(170,0,100,100),0.0f,2.0f,2.0f,ds::Color(255,128,0,32));

	m_World.addSpriteEntity(3,0,&m_HeadEntity,400,400,ds::Rect(0,0,40,40));
	m_World.addCollisionEntity(3,&m_HeadCollisionEntity);
	m_World.setCircleShape(&m_HeadCollisionEntity,20.0f,PLAYER_TYPE);

	m_World.add(3,&m_CursorEntity);
	m_CursorEntity.init(0,new ds::Sprite(400,400,ds::Rect(120,120,32,32)));

	m_World.addSpriteEntity(10,0,&m_GetReadyEntity,512,384,ds::Rect(620,0,340,50));

	m_World.addHUDEntity(12,&m_HUD,id,"xscale");
	m_HUDData.scoreIdx = m_HUD.addCounter(400,730,6,0,ds::Color::WHITE,1.5f);
	m_HUDData.score = 0;

	m_HeadPos = ds::Vec2(400,400);
	
	m_World.addParticleSystemEntity(2,id,"small_exp",&m_Particles,2000,m_AddBS);
	m_World.addParticleSystemEntity(2,id,"ring_exp",&m_RingExp,2000,m_AddBS);
	m_World.addParticleSystemEntity(2,id,"bomb_exp",&m_BombExp,2000,m_AddBS);
	m_World.addParticleSystemEntity(2,id,"trail",&m_Trail,400,m_AddBS);
	m_World.addParticleSystemEntity(2,id,"head_exp",&m_HeadExp,600,m_AddBS);
	m_World.addParticleSystemEntity(2,id,"small_cube_exp",&m_SmallCubeExp,8000,m_AddBS);
	m_World.addParticleSystemEntity(2,id,"gate_exp",&m_HGateExp,600,m_AddBS);
	m_World.addParticleSystemEntity(2,id,"v_gate_exp",&m_VGateExp,600,m_AddBS);
	m_World.addNewParticleSystemEntity(2,id,"big_ring_exp",&m_OuterRingExp,20,m_AddBS);
	m_Trail.start(ds::Vec2(800,600));

	m_World.createGameObject<BombManager>(&m_Bombs);
	//m_World.createGameObject<Swarm>(&m_Swarm);
	//m_World.createGameObject<Sparkles>(&m_Sparkles);	
	m_World.createGameObject<GameTimer>(&m_PlayTime);
	m_PlayTime.stop();

	m_World.addCollisionIgnore(RING_TYPE,GATE_TYPE);
	m_World.addCollisionIgnore(BOMB_TYPE,GATE_TYPE);
	m_World.addCollisionIgnore(CUBE_TYPE,GATE_TYPE);

	// create dialogs
	m_DialogBatch = new ds::SpriteBatch(renderer,1024,m_World.getTextureID(id));
	gui.init(m_DialogBatch,renderer,"xscale",m_World.getTextureID(id));
	// game over dialog
	gui.createDialog("GameOver",&m_GameOverDlg);
	m_GameOverDlg.addImage(ds::Vec2(512,600),ds::Rect(670,0,380,45));
	m_GameOverDlg.addButton(200,"Restart",ds::Rect(800,0,320,60));
	m_GameOverDlg.addButton(300,"Main Menu",ds::Rect(800,0,320,60));
	//m_GameOverDlg.activate();

	startGame();
	return true;
}

// -------------------------------------------------------
// Move player
// -------------------------------------------------------
void Survive::movePlayer(float elapsed) {
	ds::Vec2 mp = getMousePos();	
	mp.y = 768.0f - mp.y;	
	ds::Vec2 tp = m_HeadPos;
	m_CursorEntity.setPosition(mp);
	ds::math::followRelative(mp,tp,&m_HeadAngle,2.0f,1.0f*elapsed);	
	ds::math::clamp(tp,120.0f,850.0f,110.0f,660.0f);	
	m_HeadPos = tp;
	m_HeadEntity.setPosition(m_HeadPos);
	m_HeadEntity.setRotation(m_HeadAngle);
	m_HeadRingEntity.setPosition(m_HeadPos);
	m_Camera.setPosition(m_HeadPos,0.42f);
	m_WorldPos = m_Camera.transformToWorld(m_HeadPos);
	m_Swarm.setTargetPos(m_WorldPos);
	m_Sparkles.setTargetPos(m_WorldPos);
	//renderer->debug(10,10,ds::Color::WHITE,"wp %3.2f %3.2f",m_WorldPos.x,m_WorldPos.y);
	m_Bombs.updateHead(m_WorldPos);
	m_HeadCollisionEntity.setPosition(m_WorldPos);
	m_Trail.setEmitterPosition(m_WorldPos);

	renderer->debug(10,10,ds::Color::WHITE,"Started %d",m_Swarm.getStarted());
}

// -------------------------------------------------------
// Handle collisions
// -------------------------------------------------------
void Survive::handleCollisions() {
	if ( m_World.getCollisionCounter() > 0 ) {
		ds::Collision col;
		int num = m_World.getCollisionCounter();
		LOG(logINFO) << "collisions detected: " << num;
		for ( int i = 0; i < num; ++i) {
			m_World.getCollision(i,col);
			LOG(logINFO) << "collision (" << i << ") first " << col.firstType << " second " << col.secondType;
			if ( ds::coll::containsType(col,PLAYER_TYPE) && ds::coll::containsType(col,CUBE_TYPE)) {		
				stopGame();
			}
			else if ( ds::coll::containsType(col,PLAYER_TYPE) && ds::coll::containsType(col,GATE_TYPE)) {
				LOG(logINFO) << "Player has hit gate";
				m_Bombs.activateGate(ds::coll::getIDByType(col,GATE_TYPE));
			}
			else if ( ds::coll::containsType(col,CUBE_TYPE) && ds::coll::containsType(col,BOMB_TYPE)) {
				m_HUDData.score += 50;
				m_HUD.setCounterValue(m_HUDData.scoreIdx,m_HUDData.score);
				LOG(logINFO) << "bomb killed cube";
				m_Swarm.removeByID(ds::coll::getIDByType(col,CUBE_TYPE));
				m_SmallCubeExp.start(ds::coll::getPositionByType(col,CUBE_TYPE));
				m_Sparkles.start(1,ds::coll::getPositionByType(col,CUBE_TYPE),2);
			}
			else if ( ds::coll::containsType(col,PLAYER_TYPE) && ds::coll::containsType(col,BOMB_TYPE)) {
				LOG(logINFO) << "Player got killed by bomb";
				stopGame();
			}
		}
	}
}

// -------------------------------------------------------
// Handle bomb events
// -------------------------------------------------------
void Survive::handleBombEvents() {
	if ( m_Bombs.hasEvents()) {
		const ds::EventStream& es = m_Bombs.getEvents();
		if ( es.containsType(100)) {
			for ( int i = 0; i < es.num(); ++i ) {
				const int type = es.getType(i);
				if ( type == 100 ) {
					LOG(logINFO) << "received BombExplosion Event";					
					BombExplosion be;
					es.get(i,&be);
					m_BombExp.start(be.position);
					if ( be.dir == 0 ) {
						m_HGateExp.start(be.position);
					}
					else {
						m_VGateExp.start(be.position);
					}
					audio->play("BombExplosion",100,false);											
				}
			}
		}		
	}		
}

// -------------------------------------------------------
// Handle sparkles
// -------------------------------------------------------
void Survive::handleSparkles() {
	int pickups = m_Sparkles.pickup(m_WorldPos,20.0f);
	if ( pickups > 0 ) {
		audio->play("Pickup",100);
		m_HUDData.score += 5;
		m_HUD.setCounterValue(m_HUDData.scoreIdx,m_HUDData.score);
	}
}
// -------------------------------------------------------
// Update
// -------------------------------------------------------
void Survive::update(const ds::GameTime& gameTime) {
	PR_START("Survive-Update")		
	if ( m_Mode == GET_READY ) {
		movePlayer(gameTime.elapsed);
		m_StartTimer += gameTime.elapsed;
		if ( m_StartTimer > 3.0f ) {
			m_GetReadyEntity.setActive(false);
			m_Mode = RUNNING;
		}
	}
	else if ( m_Mode == RUNNING ) {			
		movePlayer(gameTime.elapsed);		
		handleCollisions();
		handleBombEvents();		
		handleSparkles();
	}
	else if ( m_Mode == GAMEOVER ) {
		// move camera towards center
		ds::Vec2 tp = m_HeadPos;
		ds::Vec2 mp(492,384);
		if ( ds::vector::distance(tp,mp) > 2.0f ) {
			ds::math::followRelative(mp,tp,&m_HeadAngle,0.5f,1.0f*gameTime.elapsed);	
			m_HeadPos = tp;
			m_Camera.setPosition(m_HeadPos,0.42f);
		}
	}
	PR_END("Survive-Update")
}

// -------------------------------------------------------
// Stop game
// -------------------------------------------------------
void Survive::stopGame() {
	LOG(logINFO) << "Player got killed";
	m_Mode = GAMEOVER;
	audio->play("Explosion",100);
	gui.activate("GameOver");
	m_HeadEntity.setActive(false);
	m_HeadRingEntity.setActive(false);
	m_Trail.stop();
	m_Trail.setActive(false);
	m_HeadExp.start(m_WorldPos);
	m_Sparkles.clear();
	// explode all bombs
	std::vector<ds::Vec2> positions;
	m_Bombs.getActivePositions(positions);
	if ( !positions.empty() ) {
		for ( size_t i = 0; i < positions.size(); ++i ) {
			m_BombExp.start(positions[i]);
		}
	}
	m_Bombs.clear();
	positions.clear();
	// explode all aliens
	m_Swarm.getActivePosition(positions);
	LOG(logINFO) << "active positions: " << positions.size();
	for ( size_t i = 0; i < positions.size(); ++i ) {
		m_SmallCubeExp.start(positions[i]);
	}
	m_Swarm.clear();
	m_Swarm.setActive(false);
	m_PlayTime.stop();
	m_HUD.setActive(false);
	LOG(logINFO) << "play time " << m_PlayTime.getMinutes() << ":" << m_PlayTime.getSeconds();
}

// -------------------------------------------------------
// Start game
// -------------------------------------------------------
void Survive::startGame() {
	m_Bombs.reactivateAll();
	m_Swarm.clear();
	m_Swarm.setActive(true);
	gui.deactivate("GameOver");
	m_HeadPos = ds::Vec2(512,384);
	m_Camera.setPosition(m_HeadPos,0.42f);
	m_WorldPos = m_Camera.transformToWorld(m_HeadPos);
	m_Trail.start(m_WorldPos);
	m_Trail.setActive(true);	
	m_HeadEntity.setActive(true);
	m_HeadRingEntity.setActive(true);	
	m_Sparkles.clear();
	m_HUDData.score = 0;
	m_HUD.setCounterValue(m_HUDData.scoreIdx,m_HUDData.score);
	m_HUD.setActive(true);
	m_PlayTime.reset();
	m_PlayTime.start();
	m_Mode = GET_READY;
	m_GetReadyEntity.setActive(true);
	m_StartTimer = 0.0f;
}

// -------------------------------------------------------
// OnGUIButton
// -------------------------------------------------------
void Survive::onGUIButton( ds::DialogID dlgID,int button ) {
	LOG(logINFO) << "GUI button clicked - dlgID: " << dlgID << " button: " << button;
	if ( dlgID == 0 && button == 0 ) {
		startGame();
	}
}

// -------------------------------------------------------
// OnChar
// -------------------------------------------------------
void Survive::OnChar( char ascii,unsigned int keyState ) {	
	if ( ascii == 'a' ) {
		//m_Particles.start(ds::Vec2(800,600));
		//m_RingExp.start(ds::Vec2(800,600));
		m_VGateExp.start(ds::Vec2(800,600));
	}
	if ( ascii == 's' ) {
		m_HGateExp.start(ds::Vec2(800,600));
	}
	if ( ascii == 'b' ) {		
		m_BombExp.start(ds::Vec2(800,600));
	}
	if ( ascii == 'o' ) {		
		m_OuterRingExp.start(ds::Vec2(800,600));
	}
	if ( ascii == 'w' ) {		
		m_Particles.start(ds::Vec2(800,600));
	}
	if ( ascii == 'e' ) {		
		m_SmallCubeExp.start(ds::Vec2(800,600));
	}
	if ( ascii == 'c' ) {
		m_World.togglePause();
	}
	if ( ascii == 'd' ) {
		m_World.debug();
	}
}



