#pragma once
#include <world\World.h>

class GameTimer : public ds::GameObject {

public:
	GameTimer() {
		reset();
	}
	~GameTimer() {}
	void init() {
		reset();
	}
	void start() {
		setActive(true);
	}
	void stop() {
		setActive(false);
	}
	void reset() {
		m_Total = 0.0f;
		m_Timer = 0.0f;
		m_SecTimer = 0.0f;
		m_Seconds = 0;
		m_Minutes = 0;
		m_Hours = 0;
	}
	void update(float elapsed);
	bool hasElapsed(float threshold,bool reset = true);
	const int getSeconds() const {
		return m_Seconds;
	}
	const int getMinutes() const {
		return m_Minutes;
	}
	const int getHours() const {
		return m_Hours;
	}
private:
	float m_Total;
	float m_Timer;
	float m_SecTimer;
	int m_Seconds;
	int m_Minutes;
	int m_Hours;	
};

