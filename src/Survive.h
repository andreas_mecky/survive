#pragma comment(lib, "Diesel2.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxerr.lib")
#pragma warning(disable : 4995)

#pragma once
#include "base\BaseApp.h"
#include "dxstdafx.h"
#include <renderer\render_types.h>
#include <nodes\SpriteBatch.h>
#include <world\World.h>
#include "BombManager.h"
#include "Swarm.h"
#include "Sparkles.h"
#include <world\HUDEntity.h>
#include "GameTimer.h"
#include <world\SpriteEntity.h>

class Survive : public ds::BaseApp {

enum GameMode {
	GET_READY,
	RUNNING,
	GAMEOVER
};

public:	
	Survive();
	virtual ~Survive();
	bool loadContent();
	const char* getTitle() {
		return "Survive";
	}
	void update(const ds::GameTime& gameTime);
	void draw(const ds::GameTime& gameTime) {}
	void onGUIButton( ds::DialogID dlgID,int button );
private:
	void startSwarms(int swarms);
	void stopGame();
	void startGame();
	void movePlayer(float elapsed);
	void handleCollisions();
	void handleBombEvents();
	void handleSparkles();
	virtual void OnChar( char ascii,unsigned int keyState );
	GameSettings m_GameSettings;
	ds::SpriteEntity m_Background;
	ds::SpriteEntity m_HeadEntity;
	ds::SpriteEntity m_HeadRingEntity;
	ds::SpriteEntity m_CursorEntity;
	ds::SpriteEntity m_GetReadyEntity;
	ds::ParticlesystemEntity m_Particles;
	ds::ParticlesystemEntity m_RingExp;
	ds::ParticlesystemEntity m_BombExp;
	ds::ParticlesystemEntity m_Trail;
	ds::ParticlesystemEntity m_HeadExp;
	ds::ParticlesystemEntity m_SmallCubeExp;
	ds::ParticlesystemEntity m_VGateExp;
	ds::ParticlesystemEntity m_HGateExp;
	ds::NewParticlesystemEntity m_OuterRingExp;
	ds::CollisionEntity m_HeadCollisionEntity;
	ds::HUDEntity m_HUD;
	HUDData m_HUDData;
	ds::SpriteBatch* m_DialogBatch;
	ds::BitmapFont m_Font;
	BombManager m_Bombs;
	Sparkles m_Sparkles;
	ds::Vec2 m_HeadPos;
	float m_HeadAngle;
	ds::Camera2D m_Camera;
	Swarm m_Swarm;
	uint32 m_AddBS;
	ds::Vec2 m_WorldPos;
	GameMode m_Mode;
	//GameTimer m_ColorTimer;
	GameTimer m_PlayTime;
	float m_StartTimer;

	ds::GUIDialog m_GameOverDlg;
};