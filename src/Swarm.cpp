#include "Swarm.h"
#include "Constants.h"

Swarm::Swarm() : GameObject() , m_SeparationBehaviour(30.0f) , m_TargetBehaviour(120.0f) , m_Waves(0) , m_StartCount(1) , m_Started(0) , m_Level(0) , m_Target(0) {
	for ( int i = 0; i < 4; ++i ) {
		m_Sprites[i] = 0;
	}
}

Swarm::~Swarm(void) {
	if ( m_Target != 0 ) {
		delete m_Target;
	}
	for ( int i = 0; i < 4; ++i ) {
		if ( m_Sprites[i] != 0 ) {
			delete m_Sprites[i];
		}
	}
}

// -------------------------------------------------------
// Init
// -------------------------------------------------------
void Swarm::init() {
	m_GameSettings = static_cast<GameSettings*>(m_World->getSettings("game"));
	m_Sprites[0] = new ds::Sprite(ds::Vec2(0,0),ds::Rect(270,0,30,30));
	m_Sprites[1] = new ds::Sprite(ds::Vec2(0,0),ds::Rect(270,40,30,30));
	m_Sprites[2] = new ds::Sprite(ds::Vec2(0,0),ds::Rect(270,80,30,30));
	m_Sprites[3] = new ds::Sprite(ds::Vec2(0,0),ds::Rect(270,120,30,30));

	m_Target = new Alien;
	ds::Vec2 initialPos(400,200);
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		alien->id = i;
		m_World->addSpriteEntity(0,"blob1",0,&alien->entity);		
		alien->entity.setPosition(initialPos);
		alien->position = alien->entity.getPosition();
		alien->active = false;
		alien->entity.setActive(false);
		alien->type = -1;
		m_World->setCircleShape(&alien->entity,15.0f,CUBE_TYPE);
	}	
}

// -------------------------------------------------------
// Start
// -------------------------------------------------------
void Swarm::start(int type) {
	ds::Vec2 p(0,0);
	int idx = -1;
	ds::Vec2 startPos = START_POS[type];
	int arrayIdx = 0;
	LOG(logINFO) << "starting swarm - type " << type;
	for ( int y = 0; y < 5; ++y ) {		
		for ( int x = 0; x < 5; ++x ) {
			int flag = FILL_ARRAY[type][arrayIdx];
			if ( flag == 1 ) {
				p.x = startPos.x + x * 50.0f; 
				p.y = startPos.y - y * 50.0f;
				idx = findFreeAlien();
				if ( idx != -1 ) {
					Alien* alien = &m_Aliens[idx];
					alien->timer = 0.0f;
					alien->state = AS_GROWING;
					alien->type = ds::math::random(0,3);
					alien->entity.setSprite(m_Sprites[alien->type]);					
					alien->entity.setPosition(p);					
					alien->position = alien->entity.getPosition();
					alien->active = true;
					alien->entity.setActive(true);					
				}
				else {
					LOG(logINFO) << "No more aliens available";
				}
			}
			++arrayIdx;
		}
	}
}

// -------------------------------------------------------
// Clear
// -------------------------------------------------------
void Swarm::clear() {
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		alien->active = false;
		alien->entity.setActive(false);		
	}
	m_Waves = 0;
	m_StartCount = 1;
	m_Level = 0;
}
// -------------------------------------------------------
// Find free alien
// -------------------------------------------------------
int Swarm::findFreeAlien() {
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		if ( !alien->active ) {
			return i;
		}
	}
	return -1;
}

// -------------------------------------------------------
// Get active positions
// -------------------------------------------------------
void Swarm::getActivePosition(std::vector<ds::Vec2>& positions) {
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		if ( alien->active ) {
			positions.push_back(alien->position);
		}
	}
}
// -------------------------------------------------------
// numAlive
// -------------------------------------------------------
int Swarm::numAlive() {
	int ret = 0;
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		if ( alien->active ) {
			++ret;
		}
	}
	return ret;
}

// -------------------------------------------------------
// Update
// -------------------------------------------------------
void Swarm::update(float elapsed) {
	m_Target->position = m_TargetPos;
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		if ( alien->active ) {
			if ( alien->state == AS_MOVING ) {
				ds::Vec2 p = alien->entity.getPosition();
				ds::Vec2 velocity(0,0);
				if ( m_TargetBehaviour.update(alien,m_Target) ) {
					velocity += m_TargetBehaviour.getReaction();
				}
				/*
				if ( m_FleeBehaviour.update(alien,m_Target)) {
					velocity += m_FleeBehaviour.getReaction();
				}
				*/
				float angle = ds::math::getAngle(velocity,ds::V2_RIGHT);

				Alien* closest = findClosest(*alien);
				if ( m_SeparationBehaviour.update(alien,closest)) {
					velocity += m_SeparationBehaviour.getReaction();
				}
				ds::vector::addScaled(p,velocity,elapsed);
				alien->position = p;
				alien->entity.setRotation(angle);								
				alien->entity.setPosition(p);
			}
			else if ( alien->state == AS_GROWING ) {
				alien->timer += elapsed;
				float scale = 0.2f + alien->timer / m_GameSettings->cubeGrowTime * 0.8f;
				alien->entity.setScale(scale,scale);
				if ( alien->timer > m_GameSettings->cubeGrowTime ) {
					alien->state = AS_MOVING;
				}
			}
		}
	}

	int waves = numAlive() / 8;
	int diff = m_Waves - waves;
	if ( diff > 0 ) {
		LOG(logINFO) << "--- diff " << diff;
		m_Waves = waves;
		startGroups(diff);
	}
	if ( numAlive() < 4 ) {
		startGroups(m_StartCount);
	}
	int test = m_Started / 10;
	if ( test != m_Level ) {
		++m_StartCount;
		LOG(logINFO) << "--- setting start count: " << m_StartCount;
		m_Level = test;
	}	
}

// -------------------------------------------------------
// Find closest alien
// -------------------------------------------------------
Alien* Swarm::findClosest(const Alien& currentAlien) {
	float minDist = 1000.0f;
	Alien* retAlien = 0;
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		if ( alien->id != currentAlien.id && alien->active ) {
			float dist = ds::vector::distance(alien->position,currentAlien.position);
			if ( dist < minDist ) {
				minDist = dist;
				retAlien = alien;
			}
		}
	}
	if ( minDist > 60.0f ) {
		retAlien = 0;
	}
	return retAlien;
}

// -------------------------------------------------------
// Remove by ID
// -------------------------------------------------------
void Swarm::removeByID(int entityID) {
	for ( int i = 0; i < MAX_SWARM; ++i ) {
		Alien* alien = &m_Aliens[i];
		if ( alien->entity.getID() == entityID ) {
			alien->active = false;
			alien->entity.setActive(false);
		}
	}
}

// -------------------------------------------------------
// Start swarms
// -------------------------------------------------------
void Swarm::startGroups(int swarms) {
	int all[8];
	for ( int i = 0; i < 8; ++i ) {
		all[i] = i;
	}
	for ( int i = 0; i < 20; ++i ) {
		int f = ds::math::random(0,7);
		int s = ds::math::random(0,7);
		if ( f != s ) {
			int tmp = all[f];
			all[f] = all[s];
			all[s] = tmp;			
		}
	}	
	m_Waves += swarms;
	m_Started += swarms;
	for ( int i = 0; i < swarms; ++i ) {
		start(all[i]);
	}	
}