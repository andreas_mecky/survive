#include "Sparkles.h"

//const ds::Color SPK_COLOR[] = {ds::Color(0.8f,0.0f,0.0f,1.0f),ds::Color(0.0f,0.8f,0.0f,1.0f),ds::Color(0.8f,0.4f,0.0f,1.0f),ds::Color(0.0f,0.25f,0.8f,1.0f)};
const ds::Color SPK_COLOR = ds::Color(0.8f,0.8f,0.0f,1.0f);

Sparkles::Sparkles(void) : GameObject() , m_TargetPos(0,0) {
}


Sparkles::~Sparkles(void) {
}

// -------------------------------------------------------
// Init
// -------------------------------------------------------
void Sparkles::init() {
	m_GameSettings = static_cast<GameSettings*>(m_World->getSettings("game"));
	assert(m_GameSettings != 0);
	for ( int i = 0; i < MAX_SPARKLES; ++i ) {
		Sparkle* sparkle = &m_Sparkles[i];
		m_World->addSpriteEntity(0,"sparkle",0,&sparkle->entity);
		sparkle->entity.setActive(false);
		sparkle->type = -1;
	}
}

// -------------------------------------------------------
// Update
// -------------------------------------------------------
void Sparkles::update(float elapsed) {
	for ( int i = 0; i < MAX_SPARKLES; ++i ) {
		Sparkle* sparkle = &m_Sparkles[i];
		if ( sparkle->type != -1 ) {
			ds::Vec2 p = sparkle->entity.getPosition();
			ds::Vec2 diff = m_TargetPos - p;
			if ( ds::vector::length(diff) < m_GameSettings->sparklePickupRadius ) {
				ds::Vec2 nd = ds::vector::normalize(diff);
				nd *= m_GameSettings->sparkleTargetVelocity;
				ds::vector::addScaled(p,nd,elapsed);
			}
			else {
				ds::vector::addScaled(p,sparkle->velocity,elapsed);
			}
			sparkle->entity.setPosition(p);
			// check bounds and set type = -1
			if ( p.x < 170.0f || p.x > 1200.0f || p.y < 156.0f || p.y > 940.0f ) {
				sparkle->type = -1;
				sparkle->entity.setActive(false);
			}
		}
	}
}

// -------------------------------------------------------
// find free sparkle
// -------------------------------------------------------
int Sparkles::findFreeSparkle() {
	for ( int i = 0; i < MAX_SPARKLES; ++i ) {
		Sparkle* sparkle = &m_Sparkles[i];
		if ( sparkle->type == -1 ) {
			return i;
		}
	}
	return -1;
}

// -------------------------------------------------------
// Clear all sparkles
// -------------------------------------------------------
void Sparkles::clear() {
	for ( int i = 0; i < MAX_SPARKLES; ++i ) {
		Sparkle* sparkle = &m_Sparkles[i];
		sparkle->type = -1;
		sparkle->entity.setActive(false);
	}
}

// -------------------------------------------------------
// start
// -------------------------------------------------------
void Sparkles::start(int type,const ds::Vec2& pos,int count) {
	float steps = 360.0f / static_cast<float>(count);
	float angle = ds::math::random(0.0f,90.0f);
	for ( int i = 0; i < count; ++i ) {
		int idx = findFreeSparkle();
		if ( idx != -1 ) {
			Sparkle* sparkle = &m_Sparkles[idx];
			sparkle->entity.setActive(true);
			sparkle->entity.setPosition(pos);
			sparkle->entity.setColor(SPK_COLOR);
			sparkle->type = type;
			float vel = m_GameSettings->sparkleVelocity + ds::math::random(-20,20);
			sparkle->velocity = ds::math::getRadialVelocity(angle,vel);
			angle += steps;
		}
	}
}

// -------------------------------------------------------
// pickup
// -------------------------------------------------------
int Sparkles::pickup(const ds::Vec2& pos,float radius) {
	int total = 0;
	float dist = 0.0f;
	ds::Vec2 pen(0,0);		
	for ( int i = 0; i < MAX_SPARKLES; ++i ) {
		Sparkle* sparkle = &m_Sparkles[i];
		if ( sparkle->type != -1 ) {				
			if ( ds::math::checkCircleIntersection(sparkle->entity.getPosition(),5.0f,pos,radius,&dist,pen)) {
				++total;				
				sparkle->type = -1;
				sparkle->entity.setActive(false);
			}
		}
	}
	return total;
}