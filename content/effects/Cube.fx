uniform extern float4x4 gWVP;
uniform extern texture gTex;
float Timer = 0.0;
float texel = 1.0 / 1024.0;

sampler TexS = sampler_state {
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
    AddressV  = WRAP;
};

struct OutputVS {
    float4 posH   : POSITION0;
	float2 Tex   : TEXCOORD0;
	float4 color0 : COLOR0;
};

OutputVS TextureVS(float3 posL : POSITION0,float2 tex0 : TEXCOORD0 , float4 color : COLOR0) {
    OutputVS outVS = (OutputVS)0;	
	outVS.posH = mul(float4(posL, 1.0f), gWVP);		
	outVS.Tex = tex0;
	outVS.color0 = color;
	return outVS;
}

float bump(float2 inp,float sign) {
	float shift = 0.3;	
	float2 ti = inp;
	ti.x += 128.0 * texel;
	ti.x += sin(Timer * 1.0) * 32.0 * texel * sign;
	ti.y += cos(Timer * 1.0) * 32.0 * texel * sign;
	float3 bumpColor = tex2D(TexS,ti); 
	//bumpColor = ((bumpColor*2.0)-1.0)*shift;	
	return (bumpColor.r + bumpColor.g + bumpColor.b) / 3.0;
}

float4 TexturePS(OutputVS input) : COLOR {    	
	float4 texColor = tex2D(TexS,input.Tex);
	//if ( texColor.r > 0.9 ) {
		//return input.color0 * texColor.r;
	//}
	if ( texColor.a > 0.0f ) {
		float bumpColor = bump(input.Tex,1.0);	
		float4 color2 = input.color0;//tex2D(TexS,input.Tex);
		color2.r *= bumpColor;
		color2.g *= bumpColor;
		color2.b *= bumpColor;
		return color2 * texColor.a;
	}
	else {
		return float4(0,0,0,0);
	}
}

technique CubeTech {

    pass P0 {
        // Specify the vertex and pixel shader associated with this pass.
        vertexShader = compile vs_2_0 TextureVS();
        pixelShader  = compile ps_2_0 TexturePS();
		//FillMode = Wireframe;
    }
}
